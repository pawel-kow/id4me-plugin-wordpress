# ID4me

**Contributors:** [gdespoulain](https://profiles.wordpress.org/gdespoulain), [herrfeldmann](https://profiles.wordpress.org/herrfeldmann/), [kimalumalu](https://profiles.wordpress.org/kimalumalu), [pfefferle](https://profiles.wordpress.org/pfefferle), [rinma](https://profiles.wordpress.org/rinma), [ionos](https://profiles.wordpress.org/ionos)  
**Tags:** ID4me, login, domain  
**Requires at least:** 5.0  
**Requires PHP:** 7.0  
**Tested up to:** 5.2  
**Stable tag:** 1.0.2  
**License:** MIT  
**License URI:** https://gitlab.com/ID4me/id4me-plugin-wordpress/blob/LICENSE  
**Author:** 1&1 IONOS

This plugin allows users to use their domain to sign in into WordPress, through any third party of their choice.
It uses the **ID4me protocol** and requires a domain that has been registered with ID4me.

## Description/Concept

[ID4me](https://id4me.org/) is a public, open, federated digital identity service that centralizes user access and user data into one
choosen platform, without the user being subsequently bound to said platform, i.e. with the possibility for the
user to be able to switch platform without losing any connection to the services where they connect.

![ID4me: a graphic](assets/screenshots/readme-id4me-principle.png)

### How does it work?

The domain itself contains the information about which platform (any third party supporting the ID4me protocol)
it uses to log in its **DNS records**; it means, internally.

### Vocabulary

* This third party platform used to communicate with the user is called the **Identity Authority**.
* The middlepart holding the user's information where the domain is registered is called the **Identity Agent**.
* The domain is called an **identifier** and is registered in the Identity Autority through the Identity Agent.
* The identifier can then be used to log in other services, called a **Relying Party**. That's the WordPress site
where you want to install this plugin.

## How do I obtain an ID4me identifier?

In order for any domain to become an ID4me username, the domain must be registered to an identity authority
through an identity agent.

Then, the resulting DNS record must be created with the identifying data (identity authority, identity agent) in order
for the identifier to work.

**Example: Registering a domain by _denic.de_**

The [Denic Demo Identity Agent](https://identityagent.de/register) allows a domain to be registered to the authority
in just one click:

![ID4me: Domain Register Form](assets/screenshots/readme-register-form.png)

The agent then generates the necessary DNS text entries for the domain:

![ID4me: Domain Register Form](assets/screenshots/readme-register-data.png)

These two specific lines must be added to your DNS as **TXT records**.

## How do I build the plugin?

Both CSS and vendor files are not present in this repository and must be generated before the plugin can be used.

### Building the CSS

Make sure your have [**LESS**](http://lesscss.org/) installed on your machine and then run in your repo:

```
$ lessc assets/css/login.less assets/css/login.css
```

### Building the libraries

Make sure your have [**Composer**](https://getcomposer.org/doc/00-intro.md) installed on your machine
and then run in your repo:

```
$ composer install
```

### In WordPress

Please refer to the instructions in _readme.txt_ for the installation in WordPress. Once you've build the CSS
and librairies following the instructions above, a compressed version (Zip) of the sources is already WordPress-compatible.

## Changelog

### 1.0.2

* Update library version to 1.1.0

### 1.0.1

* Update doc with tested up to 5.2

### 1.0.0

* Add cron job to clean up ID4me expired transients
* Add custom HTTP client
* Fix datetime compatibility with MySQL 5.5
* Update library version to 1.0.0

### 0.1.1

* Implement WP.org feedback: use appropriate sanitize() instead of esc() for sanitizing input in user settings

### 0.1.0

* Initial