<?php
/**
 * Class ID4me_Login
 */
class ID4me_Login extends ID4me_Env {
    /**
     * Render the extra ID4me login form
     * @action login_footer
     */
    public function login_form() {
        global $interim_login;
        if ($this->is_login_page()) {
            $login_html = '';
            // Build the URL switching back to the standard WP login
            $wp_redirect_to = !empty($_GET['redirect_to']) ? esc_url($_GET['redirect_to']) : '';
            $login_html.= '
				<p class="forgetmenot-wp"><label for="rememberme-wp"><input name="rememberme-wp" type="checkbox" id="rememberme-wp" value="forever"> Angemeldet bleiben</label></p>
				<input type="submit" name="wp-submit-wp" id="wp-submit-id4me" class="button button-primary button-large" value="Anmelden">
				</form>
			';
            // Show the errors that our processes eventually thrown
            $login_html.= $this->show_errors();
            $login_html.= '

				<form id="id4me-loginform" class="id4me" method="get">

				<div id="id4me-button" class="id4me btn login-btn" >
				<a id="id4me-button-anchor" class="button button-large" type="button">
					<span id="id4me-text" class="login-button-text">
							' . esc_html__('Log in with ID4me', 'id4me') . '
					</span>
				</a>
					<div class="outer" id="id4me-handler">
						<div class="inner" id="edit">
						</div>
					</div>

				</div>


				<input type="hidden" name="id4me_action" value="connect">


				<div id="id4me-handler-form" class="hiddenform">
					<label for="id4me-identifier" id="id4me-identifier">
						' . esc_html__('Enter your identifier:', 'id4me') . '<br>
						<input type="text"
								name="id4me_identifier"
								id="id4me-input">
					</label>

				<label for="id4me-checkbox" id="id4me-rememberme">
				<input id="id4me-checkbox" type="checkbox" name="rememberme" value="yes">
								' . esc_html__('merken') . '
				</label>

				<label for="id4me-input-signin" id="id4me-signin">
				<button type="submit" name="id4me_login_submit" id="id4me-input-signin" class="id4me-login-submit id4me-button button button-primary button-large">
						' . esc_html__('Anmelden') . '
				</button>
				</label>

				</div>
				</form>

				<div id="id4me-wp">
					<a type="button" id="id4me-wp-button" class="id4me-wp-bt id4me-button button button-primary button-large">
							' . esc_html__('Or Log in with Wordpress') . '
					</a>
				</div>


			';
            echo $login_html;
        }
    }
    /**
     * Insert an "id4me" extra body class to differentiate WP login / ID4me login
     * @action login_body_class
     *
     * @param  array $classes
     * @return array
     */
    public function login_body_class($classes) {
        // Are we on the ID4me login page?
        if (!empty($_GET['id4me_action'])) {
            $classes[] = 'id4me';
        }
        return $classes;
    }
    /**
     * Check if we are in the WP login page
     *
     * @return boolean
     */
    public function is_login_page() {
        return !isset($_REQUEST['action']) || ('login' === $_REQUEST['action']);
    }
    /**
     * Add the alternative login CSS/JS scripts
     * @action login_enqueue_scripts
     */
    public function enqueue_scripts() {
        if ($this->is_login_page()) {
            // Additional CSS
            echo '<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>';
            wp_enqueue_style('id4me-login', plugin_dir_url(__DIR__) . 'assets/css/login.css');
            wp_enqueue_style('id4me-button', plugin_dir_url(__DIR__) . 'assets/new/style.css');
            wp_enqueue_script('id4me-javascript', plugin_dir_url(__DIR__) . 'assets/new/javascript.js');
        }
    }
}
