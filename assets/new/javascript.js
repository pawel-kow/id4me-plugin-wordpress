function setCookie(cname, cvalue) {
	document.cookie = cname + "=" + encodeURI(cvalue) + ";path=/";
}

function deleteCookie( name ) {
	var string = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
	document.cookie = string;
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i < ca.length; i++) {
	  var c = ca[i];
	  while (c.charAt(0) == ' ') {
		c = c.substring(1);
	  }
	  if (c.indexOf(name) == 0) {
		return decodeURI(c.substring(name.length, c.length));
	  }
	}
	return "";
}


function redirectURL (event){
	event.preventDefault();

		if($('#id4me-checkbox').prop('checked')){
			setCookie("id4me.user", $('#id4me-input').val());
		}

		// spinner animation
		$('#id4me-input-signin').html("Login");
		// disable button and enter key
		$('#id4me-input').off("keydown");
		$('#id4me-input-signin').html("<div class='loader' id='id4me-loader'></div>");
		$('.loader').css("display", "inline-block");
		$('#id4me-loginform').submit();
}

$(function() {
	$('#id4me-input').keyup(function(event) {
	  // Number 13 is the "Enter" key on the keyboard
	  if (event.keyCode === 13) {
			$(selector).on("change keyup blur input", function() {
					return false;
			});
			redirectURL(event);
	  }
	});

	$('#id4me-input-signin').click(function(event){
		redirectURL (event);
	});
});



$(document).ready(function(){
	var user = getCookie("id4me.user");

	if (user != "") {
			//Settings with Cookie
					$('#id4me-input').attr("type", "hidden");
					$('#id4me-input').attr("value", user);
				  $('#id4me-text').html(" Login as " + user);
				  $('#id4me-handler').show();
			} else {
			//Settings wihout Cookie set
				  $('#id4me-handler').hide();
			}

			//OnClick Handler Item click
			$('#id4me-handler').click(function() {
					if (user != "") {
					//ID4me Form exist
					if($("#id4me-handler-form").is(":visible")){
							$('body.assistant-page #loginform').fadeIn('slow');
							$('#id4me-handler-form').fadeOut('fast');
							$('.inner').attr("id", "edit");//handler-style to save
							$('body.assistant-page #id4me-wp').fadeOut('fast');
							// set cookie if checked
							if($('#id4me-checkbox').prop('checked') && $('#id4me-input').val()!=""){
								setCookie("id4me.user", $('#id4me-input').val());
								user = getCookie("id4me.user");
							  $('#id4me-text').html(" Login as " + user);
							}else{
								deleteCookie("id4me.user");
								user = "";
								$('#id4me-input').attr("value", user);
								$('#id4me-text').html("Log in with ID4me");
								$('#id4me-handler').hide();
							}
					}else{
							$('body.assistant-page #loginform').fadeOut('fast');
							$('#id4me-handler-form').fadeIn('fast');
							$('#id4me-handler-form').css("display", "grid");
							$('#id4me-input').focus();
							$('#id4me-input').attr("type", "text");
							$('#id4me-input').attr("value", user);
							$('#id4me-text').html("Log in with ID4me"); //Should be translated (example: . esc_html__('Log in with ID4me', 'id4me') . ) but dont know how
							$('.inner').attr("id", "save");//handler-style to save
							$('body.assistant-page #id4me-wp').fadeIn('slow');
					}
				}else{
				}
			});

			//OnClick Button Item click
			$('#id4me-button-anchor').click(function() {
				if (user != "") {
					if($("#id4me-handler-form").is(":visible")){
					}else{
						$('#id4me-loginform').submit();
					}
				}else{
					if($("#id4me-handler-form").is(":visible")){
							$('#id4me-handler-form').fadeOut('fast');
							$('body.assistant-page #loginform').fadeIn('slow');
							$('body.assistant-page #id4me-wp').fadeOut('fast');
					} else {
							$('#id4me-handler-form').fadeIn('slow');
							$('#id4me-handler-form').css("display", "grid");
							$('body.assistant-page #loginform').fadeOut('fast');
							$('body.assistant-page #id4me-wp').fadeIn('slow');
							$('#id4me-input').focus();
					}
				}
			});

			//OnClick BackToWPLink Item click
			$('body.assistant-page #id4me-wp').click(function() {
				if (user != "") {
					$('#id4me-handler-form').fadeOut('fast');
					$('body.assistant-page #loginform').fadeIn('slow');
					$('body.assistant-page #id4me-wp').fadeOut('fast');
					$('#id4me-text').html(" Login as " + user);
				}else{
					$('#id4me-handler-form').fadeOut('fast');
					$('body.assistant-page #loginform').fadeIn('slow');
					$('body.assistant-page #id4me-wp').fadeOut('fast');
					}
			});

});
